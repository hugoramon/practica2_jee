package control;

import model.DBInteraction;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name = "Login", urlPatterns = {"/register"})
public class Register extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession s= req.getSession(false);
        if(s != null && s.getAttribute("valid") != null) { resp.sendRedirect(resp.encodeURL("list")); return; }
        RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/register.jsp");
        rd.forward(req,resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("verify") == null) {
            String name = req.getParameter("name");
            String nick = req.getParameter("nick");
            String password = req.getParameter("pass");
            String surname = req.getParameter("surname");
            String address = req.getParameter("address");
            String phone = req.getParameter("phone");
            HttpSession ses = req.getSession(true);
            //Set session attributes
            ses.setAttribute("nick",nick);
            ses.setAttribute("name", name);
            ses.setAttribute("pass", password);
            ses.setAttribute("surname", surname);
            ses.setAttribute("address", address);
            ses.setAttribute("phone", phone);
            //Set info for response
            req.setAttribute("url",resp.encodeURL("register"));
            req.setAttribute("urlBack",resp.encodeURL("register"));
            req.setAttribute("validate",true);
            RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/register.jsp");
            rd.forward(req,resp);
        } else {
            HttpSession ses = req.getSession();
            if(ses == null) { req.getRequestDispatcher("WEB-INF/register.jsp").forward(req,resp); }
            try {
                DBInteraction db = new DBInteraction();
                db.addusr(
                        (String) ses.getAttribute("nick"),
                        (String) ses.getAttribute("pass"),
                        (String) ses.getAttribute("name"),
                        (String) ses.getAttribute("surname"),
                        (String) ses.getAttribute("address"),
                        (String) ses.getAttribute("phone"));
                ses.removeAttribute("pass");
                ses.removeAttribute("surname");
                ses.removeAttribute("address");
                ses.removeAttribute("phone");
                ses.removeAttribute("name");

                ses.setAttribute("valid",true);
                resp.sendRedirect(resp.encodeURL("list"));
            } catch (Exception throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
