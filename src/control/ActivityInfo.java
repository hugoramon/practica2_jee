package control;

import model.Activity;
import model.DBInteraction;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;


@WebServlet(name = "Activity info", urlPatterns = {"/activity"})
public class ActivityInfo extends AuthHttpServlet{
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
        if(!isSessionIsValid()) {return;}
        DBInteraction db = null;
        HttpSession s = req.getSession();
        try {
            db = new DBInteraction();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        String id = req.getParameter("id");
        req.setAttribute("listUrl",resp.encodeURL("list"));
        try {
            if (id != null) {
                int activityID = Integer.parseInt(id);
                Activity activity = db.getActivity(activityID);
                if (activity == null ){
                    req.setAttribute("error", "The activity does not exist");
                } else {
                    req.setAttribute("activity",activity);
                }

            } else {
                req.setAttribute("error", "Value not passed");
            }
        }catch(Exception e){
            e.printStackTrace();
            req.setAttribute("error", "An error has ocurred");
        }
        RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/activityinfo.jsp");
        rd.forward(req,resp);
    }
}
