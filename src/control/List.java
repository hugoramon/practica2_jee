package control;

import model.Activity;
import model.DBInteraction;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.sql.*;
import java.util.ArrayList;

@WebServlet(name = "list", urlPatterns = {"/list"})
public class List extends AuthHttpServlet {

	@Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException, ServletException{
    	super.doGet(req,resp);
    	if(!isSessionIsValid()) {return;}
		req.setAttribute("urlList",resp.encodeURL("list"));
		req.setAttribute("urlModAct",resp.encodeURL("modact"));
		req.setAttribute("urlAct",resp.encodeURL("activity"));
		req.setAttribute("method","get");
		RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/list.jsp");
		rd.forward(req,resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
		super.doPost(req,resp);
		DBInteraction db = null;
		if(!isSessionIsValid()) {return;}
		req.setAttribute("urlList",resp.encodeURL("list"));
		req.setAttribute("urlModAct",resp.encodeURL("modact"));

		PrintWriter out = resp.getWriter();
		req.setAttribute("method","post");
		HttpSession session = req.getSession();
		try {
			db = new DBInteraction();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		try{
	        String type=req.getParameter("type");
	        String text=req.getParameter("text1");
	        String order=req.getParameter("order");
	        //req.setAttribute("type",type);

			System.out.println(order);
	        switch(type) {
	        	case "all_activities":
			}
			if (type.equals("all_activities")){
				ArrayList<Activity> a = db.listallact(order);
				req.setAttribute("list",db.listallact(order));
	        }
	        else if (type.equals("all_pavillions")){
				req.setAttribute("list",db.listAllPavs(order));
	        }
			else if (type.equals("free_places")){
				req.setAttribute("list",db.listactfreeplaces(order));
	        }
			else if (type.equals("cost")){
				req.setAttribute("list",db.listactprice(Integer.parseInt(text),order));
	        }
			else if(type.equals("sub_actvs")){
				req.setAttribute("list",db.listScribedActs((String) session.getAttribute("nick"),order));
			}
			else{
				req.setAttribute("list",db.listactfreeplaces(order));
	     	}

			RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/list.jsp");
			rd.forward(req,resp);
			db.close();
	    }  //try end
	    catch (Exception e){
	        System.err.println(e.getMessage());
	        req.setAttribute("error","An error have occurred");
	        req.removeAttribute("method");
			RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/list.jsp");
			rd.forward(req,resp);
			try {
				db.close();
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
	 }//doPost end

 }//class end