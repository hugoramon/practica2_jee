package control;

import model.DBInteraction;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(name = "login", urlPatterns = {"/login"})
public class Login extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession s= req.getSession(false);
        if(s != null && s.getAttribute("valid") != null) { resp.sendRedirect(resp.encodeURL("list")); return; }
        req.setAttribute("loginUrl",resp.encodeURL("login"));
        RequestDispatcher ds = req.getRequestDispatcher("WEB-INF/login.jsp");
        ds.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession s= req.getSession(false);
        if(s != null && s.getAttribute("valid") != null) { resp.sendRedirect(resp.encodeURL("list")); return; }
        //Get parameters
        String nick = req.getParameter("nick");
        String pass = req.getParameter("pass");
        //Verify that the user exists
        try {
            DBInteraction db = new DBInteraction();
            s = req.getSession(true);
            if(db.authentication(nick,pass)) {
                s.setAttribute("valid",true);
                s.setAttribute("nick",nick);
                resp.sendRedirect(resp.encodeURL("list"));
            } else {
                Integer tries = (Integer) s.getAttribute("tries");
                req.setAttribute("loginUrl",resp.encodeURL("login"));
                if(tries == null) { s.setAttribute("tries",1); tries = 0; } else { s.setAttribute("tries", tries+1);}
                if (tries == 3) {
                    s.removeAttribute("tries");
                    resp.sendRedirect("/");
                } else {
                    req.setAttribute("error",true);
                    RequestDispatcher ds = req.getRequestDispatcher("WEB-INF/login.jsp");
                    ds.forward(req,resp);
                }

            }
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }
    }
}
