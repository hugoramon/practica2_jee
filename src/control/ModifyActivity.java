package control;

import model.DBInteraction;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "Register", urlPatterns = {"/modact"})
public class ModifyActivity extends AuthHttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
        if(!isSessionIsValid()) {return;}
        DBInteraction db = null;
        HttpSession s = req.getSession();
        try {
             db = new DBInteraction();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        String action = req.getParameter("action");
        req.setAttribute("retunUrl",resp.encodeURL("list"));
        try {
            if(action.equals("subscribe")) {
                db.regactivity((String)s.getAttribute("nick"),req.getParameter("id"));
                req.setAttribute("message","Successfully suscribed to the activity");
            } else if(action.equals("unsubscribe")){
                db.unregactivity((String)s.getAttribute("nick"),req.getParameter("id"));
                req.setAttribute("message","Successfully unsubscribed to the activity");
            }
        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("error","An error has occurred, check that you are not already suscribed and if the activity exists");
        }
        RequestDispatcher rd = req.getRequestDispatcher(resp.encodeURL("WEB-INF/modact.jsp"));
        rd.forward(req,resp);
    }
}
