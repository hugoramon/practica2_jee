package control;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "Home", urlPatterns = {""})
public class Home extends  AuthHttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession s= req.getSession(false);
        if(s != null && s.getAttribute("valid") != null) { resp.sendRedirect(resp.encodeURL("list")); return; }

        RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/home.jsp");
        rd.forward(req,resp);
    }
}
