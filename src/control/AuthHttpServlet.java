package control;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthHttpServlet extends HttpServlet {
    private boolean sessionIsValid = false;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        checkSession(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        checkSession(req, resp);
    }

    public void checkSession(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //Check if the user is authorized otherwise sent him to the login screen
        HttpSession s = req.getSession(false);
        if(s == null || s.getAttribute("valid") == null) {
            resp.sendRedirect("login");
            setSessionIsValid(false);

        } else {
            setSessionIsValid(true);
        }
    }

    public boolean isSessionIsValid() { return sessionIsValid; }

    public void setSessionIsValid(boolean sessionIsValid) { this.sessionIsValid = sessionIsValid; }
}
