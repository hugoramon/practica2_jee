package model;

import java.sql.*;
import java.util.ArrayList;

public class DBInteraction {

    private static final String dblogin = "admin"; //root
    private static final String dbpasswd = "123456789"; //dat14

    //private static final String dblogin = "root";
    //private static final String dbpasswd = "dat14";
    Query q;
    Connection con;

    //Constructor that connects to the Database
    public DBInteraction() throws SQLException {
        String url = "jdbc:mysql://localhost/gestor_actividades";
        url = "jdbc:mariadb://localhost/practicajee";
        try {
            //Class.forName("com.mariadb.jdbc.Driver");
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (java.lang.ClassNotFoundException e) {
            System.err.print("ClassNotFoundException: ");
            System.err.println(e.getMessage());
        }
        try {
            System.out.println("Trying to connect...");
            con = DriverManager.getConnection(url, dblogin, dbpasswd);
            System.out.println("Connected!");
            q = new Query(con);
        } catch (SQLException ex) {
            System.err.print("SQLException: ");
            System.err.println(ex.getMessage());
        }
    }

    //Method to close the database Connection
    public void close() throws Exception {
        q.close();
        con.close();
    }

    //Method to add a new user to the CLIENTS table
    public void addusr(String login, String pwd, String name, String surname, String address, String phone)
            throws Exception {
        PreparedStatement ps =  con.prepareStatement("INSERT INTO CLIENTS VALUES (?,?,?,?,?,?)");
        ps.setString(1,login);
        ps.setString(2,pwd);
        ps.setString(3,name);
        ps.setString(4,surname);
        ps.setString(5,address);
        ps.setString(6,phone);
        q.doUpdate(ps);
    }

    // This method returns 'true' in case there exists a row in the CLIENTS table with the login and password passed as parameters
    // If there is no such row exists then it returns 'false'
    public boolean authentication(String login, String pwd) throws Exception {
        String list = "SELECT * FROM CLIENTS WHERE LOGIN='" + login + "'";
        PreparedStatement ps = con.prepareStatement("SELECT * FROM CLIENTS WHERE LOGIN=?");
        ps.setString(1,login);
        String password = null; //TODO ??????
        ResultSet rs = q.doSelect(ps); //rs will contain the row with login passed as parameter
        if (rs.next()) { //Check if the Resultset is empty
            password = rs.getString(2);
        }
        if (password == null) {
            return (false);
        }
        if (password.equals(pwd)) { // In case the password for this login in the table is the same as the one passed as parameter
            return (true);
        } else {
            return (false);
        }
    }

    //This method deletes the user whose login is passed as a parameter from the CLIENTS table.
    public void delusr(String login) throws Exception {
        PreparedStatement ps = con.prepareStatement("DELETE FROM CLIENTS WHERE LOGIN=?");
        ps.setString(1,login);
        q.doUpdate(ps);
    }

    //This method adds a new activity in the ACTIVITIES table using the data passed as parameters
    public void addact(String name, String description, String initial, float price, String pav_name, int total, int occ) throws Exception {
        PreparedStatement ps = con.prepareStatement("INSERT INTO ACTIVITIES (NAME, DESCRIPTION, START_DATE, COST, PAVILLION_NAME, TOTAL_PLACES, OCCUPIED_PLACES) VALUES (?,?,?,?,?,?,?)");
        ps.setString(1,name);
        ps.setString(2,description);
        ps.setString(3,initial);
        ps.setFloat(4,price);
        ps.setString(5,pav_name);
        ps.setInt(6,total);
        ps.setInt(7,occ);
        q.doUpdate(ps);
    }

    //This method deletes the row whose id is passed as a parameter from the ACTIVITIES table.
    public void delact(int id) throws Exception {
        PreparedStatement ps = con.prepareStatement("DELETE FROM ACTIVITIES WHERE ID=?");
        ps.setInt(1,id);
        q.doUpdate(ps);
    }

    //This method adds a new pavillion in the PAVILLIONS table using the data passed as parameters
    public void addpav(String pavname, String pavlocation) throws Exception {
        PreparedStatement ps  = con.prepareStatement("INSERT INTO PAVILLIONS VALUES (?,?)");
        ps.setString(1,pavname);
        ps.setString(2,pavlocation);
        q.doUpdate(ps);
    }

    //This method deletes the pavillion whose name is passed as a parameter from the PAVILLIONS table.
    public void delpav(String pavname) throws Exception {
        PreparedStatement ps  = con.prepareStatement("DELETE FROM PAVILLIONS WHERE PABELLON= ?");
        ps.setString(1,pavname);
        q.doUpdate(ps);
    }

    //This method requests the execution of an SQL sentence for listing all the clients
    //and retrieves all the information for each client, storing each client as an element
    //of an array. Each element contains an object of the type Client
    public ArrayList<Client> listallusr() throws Exception {
        ArrayList<Client> data = new ArrayList<Client>();
        PreparedStatement ps  = con.prepareStatement("SELECT * FROM CLIENTS");
        ResultSet rs = q.doSelect(ps);
        while (rs.next()) {
            String login = rs.getString(1);
            String password = rs.getString(2);
            String name = rs.getString(3);
            String surname = rs.getString(4);
            String address = rs.getString(5);
            String phone = rs.getString(6);
            data.add(new Client(login, password, name, surname, address, phone));
        }
        return (data);
    }

    //This method is common to all the listing activities operations
    //It requests the execution of a SQL sentence for listing activities depending on some criterion
    //and retrieves all the information for each activity, storing each activity as an element
    //of an ArrayList (of Activity objects)
    public ArrayList<Activity> parseActvs(PreparedStatement ps) throws Exception {
        ArrayList<Activity> data = new ArrayList<>();
        ResultSet rs = q.doSelect(ps);
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            String description = rs.getString(3);
            String initial = rs.getString(4);
            float cost = rs.getFloat(5);
            String pavname = rs.getString(6);
            int total = rs.getInt(7);
            int occupied = rs.getInt(8);
            data.add(new Activity(id, name, description, initial, cost, pavname, total, occupied));
        }
        return (data);
    }

    public ArrayList listActivitiesSmallSmall(PreparedStatement ps) throws Exception {
        ArrayList data = new ArrayList();
        ResultSet rs= q.doSelect(ps);
        while (rs.next()) {
            String name = rs.getString(1);
            int freePlaces = rs.getInt(2);
            Activity activity = new Activity();
            activity.setname(name);
            activity.setFreePlaces(freePlaces);
            data.add(activity);
        }
        return (data);
    }

    public ArrayList listActivitiesSmall(PreparedStatement ps) throws Exception{
        ArrayList data = new ArrayList();
        ResultSet rs= q.doSelect(ps);
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            int freePlaces = rs.getInt(3);
            Activity activity = new Activity();
            activity.setname(name);
            activity.setFreePlaces(freePlaces);
            data.add(activity);
        }
        return (data);
    }
    public ArrayList<Client> listUsersNames(PreparedStatement ps) throws Exception {
        ArrayList<Client> data = new ArrayList<Client>();
        ResultSet rs = q.doSelect(ps);
        while (rs.next()) {
            String name = rs.getString(1);
            Client c = new Client();
            c.setname(name);
            data.add(c);
        }
        return (data);
    }

    //This method builds an SQL sentence for listing all the activities
    public ArrayList<Activity> listallact() throws Exception {
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES");
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }
    public Activity getActivity(int id) throws Exception{
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES WHERE ID = ?");
        ps.setInt(1,id);
        ArrayList<Activity> data = this.parseActvs(ps);
        return data.size() == 0 ? null :  data.get(0);
    }

    public ArrayList<Activity> listallact(String order) throws Exception {
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES ORDER BY " + order);
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }

    // This method builds an SQL sentence for listing the activities that have free places
    public ArrayList<Activity> listactfreeplaces() throws Exception {
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES WHERE ACTIVITIES.TOTAL_PLACES > ACTIVITIES.OCCUPIED_PLACES");
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }

    public ArrayList<Activity> listactfreeplaces(String order) throws Exception {
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES WHERE ACTIVITIES.TOTAL_PLACES > ACTIVITIES.OCCUPIED_PLACES ORDER BY " + order);
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }

    // This method builds an SQL sentence for listing the activities that cost less than a certain amount
    public ArrayList<Activity> listactprice(float price) throws Exception {
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES, PAVILLIONS WHERE ACTIVITIES.COST <= ? AND ACTIVITIES.PAVILLION_NAME = PAVILLIONS.PAVILLION");
        ps.setFloat(1,price);
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }
    public ArrayList<Activity> listactprice(float price,String order) throws Exception {
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES, PAVILLIONS WHERE ACTIVITIES.COST <= ? AND ACTIVITIES.PAVILLION_NAME = PAVILLIONS.PAVILLION ORDER BY " + order);
        ps.setFloat(1,price);
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }

    // This method builds an SQL sentence for listing the activities that take place in a certain pavillion
    public ArrayList<Activity> listactpav(String namepav) throws Exception {
        String selection = "SELECT * FROM ACTIVITIES, PAVILLIONS WHERE ACTIVITIES.PAVILLION_NAME='" + namepav + "'AND ACTIVITIES.PAVILLION_NAME = PAVILLIONS.PAVILLION";
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES, PAVILLIONS WHERE ACTIVITIES.PAVILLION_NAME=? AND ACTIVITIES.PAVILLION_NAME = PAVILLIONS.PAVILLION");
        ps.setString(1,namepav);
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }

    // This method builds an SQL sentence for listing the activities that have a given name (should be only one!)
    public ArrayList<Activity> listactname(String nameact) throws Exception {
        String selection = "SELECT * FROM ACTIVITIES, PAVILLIONS WHERE ACTIVITIES.NAME='" + nameact + "'AND ACTIVITIES.PAVILLION_NAME = PAVILLIONS.PAVILLION";
        PreparedStatement ps = con.prepareStatement("SELECT * FROM ACTIVITIES, PAVILLIONS WHERE ACTIVITIES.NAME= ? AND ACTIVITIES.PAVILLION_NAME = PAVILLIONS.PAVILLION");
        ps.setString(1,nameact);
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }

    // This method builds an SQL sentence for listing the activities to which a specific client is subscribed
    public ArrayList<Activity> listScribedActs(String login) throws Exception {
        String selection = "SELECT ID, NAME, DESCRIPTION, START_DATE, COST, PAVILLION_NAME, TOTAL_PLACES, OCCUPIED_PLACES FROM SUBSCRIPTIONS, ACTIVITIES, PAVILLIONS WHERE SUBSCRIPTIONS.CLIENT_LOGIN= ? AND SUBSCRIPTIONS.ACTIVITY_ID = ACTIVITIES.ID AND ACTIVITIES.PAVILLION_NAME = PAVILLIONS.PAVILLION";
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setString(1,login);
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }

    public ArrayList<Activity> listScribedActs(String login,String order) throws Exception {
        String selection = "SELECT ID, NAME, DESCRIPTION, START_DATE, COST, PAVILLION_NAME, TOTAL_PLACES, " +
                "OCCUPIED_PLACES FROM SUBSCRIPTIONS, ACTIVITIES, PAVILLIONS WHERE SUBSCRIPTIONS.CLIENT_LOGIN= ? " +
                "AND SUBSCRIPTIONS.ACTIVITY_ID = ACTIVITIES.ID AND ACTIVITIES.PAVILLION_NAME = PAVILLIONS.PAVILLION " +
                "ORDER BY " + order;
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setString(1,login);
        ArrayList<Activity> data = this.parseActvs(ps);
        return (data);
    }
    //This method requests the execution of a SQL sentence for listing all the pavillions
    //and it retrieves all the information for each pavillion, storing each pavillion as an element
    //of an array. Each element contains an object of the type pavillion
    public ArrayList<Pavillion> pasrsePavs(PreparedStatement ps) throws Exception {
        ArrayList<Pavillion> data = new ArrayList<Pavillion>();
        ResultSet rs = q.doSelect(ps);
        while (rs.next()) {
            String name = rs.getString(1);
            String location = rs.getString(2);
            data.add(new Pavillion(name, location));
        }
        return (data);
    }

    public ArrayList<Pavillion> listAllPavs() throws Exception {
        String selection = "SELECT * FROM PAVILLIONS";
        PreparedStatement ps = con.prepareStatement(selection);
        return this.pasrsePavs(ps);
    }
    public ArrayList<Pavillion> listAllPavs(String order) throws Exception {
        String selection = "SELECT * FROM PAVILLIONS ORDER BY " + order;
        PreparedStatement ps = con.prepareStatement(selection);
        return this.pasrsePavs(ps);
    }

    //This method subscribes a client to a specific activity
    //(after checking that the activity has free places)
    public void regactivity(String login, String id) throws Exception {
        String regactivity = "INSERT INTO SUBSCRIPTIONS VALUES (?,?)";
        PreparedStatement ps = con.prepareStatement(regactivity);
        ps.setString(1,login);
        ps.setString(2,id);
        q.doUpdate(ps);
    }

    //This method unsubscribes a client from a specific activity.
    //(after checking that the client is indeed subscribed to the activity).
    public void unregactivity(String login, String id) throws Exception {
        String unregactivity = "DELETE FROM SUBSCRIPTIONS WHERE SUBSCRIPTIONS.CLIENT_LOGIN= ? AND SUBSCRIPTIONS.ACTIVITY_ID= ?";
        PreparedStatement ps = con.prepareStatement(unregactivity);
        ps.setString(1,login);
        ps.setString(2,id);
        q.doUpdate(ps);
    }

    /**######################## INCORPORAR FUNCIONES ##########################**/


    /**
     * Obtener las actividades cuyo nombre incluya el string pasado
     *
     * @param filterString
     * @return
     * @throws Exception
     */
    public ArrayList<Activity> listSimilarActivities(String filterString) throws Exception {
        String selection = "SELECT ACTIVITIES.ID,NAME, (TOTAL_PLACES - OCCUPIED_PLACES) as FREE_PLACES FROM ACTIVITIES " +
                "WHERE NAME LIKE CONCAT('%',?,'%')  ORDER BY FREE_PLACES";
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setString(1,filterString);
        return this.listActivitiesSmall(ps);
    }

    /**
     * Obtener las diferents actividades con un numero x de espacios libres
     *
     * @param freePlaces Numero de espacios libres
     */
    public ArrayList<Activity> listNotSimilarActivities(int freePlaces) throws Exception {
        String selection = "SELECT  NAME, SUM(TOTAL_PLACES - OCCUPIED_PLACES) as FREE_PLACES " +
                "FROM ACTIVITIES GROUP BY NAME HAVING FREE_PLACES > ?";
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setInt(1,freePlaces);
        return this.listActivitiesSmallSmall(ps);
    }

    public ArrayList<Activity> listNotSimilarActivities(int freePlaces,String order) throws Exception {
        String selection = "SELECT  NAME, SUM(TOTAL_PLACES - OCCUPIED_PLACES) as FREE_PLACES " +
                "FROM ACTIVITIES GROUP BY NAME HAVING FREE_PLACES > ? ORDER BY " + order;
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setInt(1,freePlaces);
        return this.listActivitiesSmallSmall(ps);
    }
    /**
     * Obtener las actividades en las que no estoy escrito ordenado segun el numero de espacios libres
     * @param clientLogin
     * @return
     * @throws Exception
     */
    public ArrayList<Activity> listNotSubscribedActivities(String clientLogin) throws Exception {
        String selection = "SELECT ACTIVITIES.ID, NAME, (TOTAL_PLACES - OCCUPIED_PLACES) as FREE_PLACES FROM ACTIVITIES " +
                "WHERE ID NOT IN (SELECT ACTIVITY_ID FROM SUBSCRIPTIONS WHERE CLIENT_LOGIN = ?)";
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setString(1,clientLogin);
        ArrayList<Activity> data = this.listActivitiesSmall(ps);
        return data;
    }

    /**
     * Obtener el nombre de los usuarios con los que coincido en un número mínimo de actividades
     *
     * @param clientLogin el id del cliente
     * @param number      el numero con el que coincides
     * @return
     * @throws Exception
     */
    public ArrayList<Client> listUsersWithSameNumberOfActivities(String clientLogin, int number) throws Exception {
        String selection = String.format("SELECT CLIENT_LOGIN,COUNT(CLIENT_LOGIN) as x FROM SUBSCRIPTIONS where ACTIVITY_ID IN (SELECT ACTIVITY_ID FROM SUBSCRIPTIONS "
                + "WHERE CLIENT_LOGIN = ?) and not CLIENT_LOGIN=? group by CLIENT_LOGIN having x >= ?");
        //ArrayList data = this.control.list
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setString(1,clientLogin);
        ps.setString(2,clientLogin);
        ps.setInt(3,number);
        return this.listUsersNames(ps);
    }

    /**
     *
     * @param number
     * @return
     * @throws Exception
     */
    public ArrayList<Client> listUsersByNumberOfActivities(int number) throws Exception {
        String selection = "SELECT CLIENTS.NAME,COUNT(CLIENTS.LOGIN) AS x FROM (CLIENTS INNER JOIN SUBSCRIPTIONS ON" +
                " CLIENTS.LOGIN = SUBSCRIPTIONS.CLIENT_LOGIN) GROUP BY CLIENTS.LOGIN HAVING x >= ?;";
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setInt(1,number);
        ArrayList data = this.listUsersNames(ps);
        return (data);
    }

    /**
     *
     * @param clientLogin
     * @return
     * @throws Exception
     */
    public ArrayList listActivitiesNotSuscribed(String clientLogin) throws Exception {
        String selection = "SELECT ACTIVITIES.ID,ACTIVITIES.NAME, (TOTAL_PLACES - OCCUPIED_PLACES) as FREE_PLACES FROM ACTIVITIES " +
                "INNER JOIN SUBSCRIPTIONS ON ACTIVITIES.ID = SUBSCRIPTIONS.ACTIVITY_ID WHERE SUBSCRIPTIONS.CLIENT_LOGIN  != ? ORDER BY " +
                "FREE_PLACES";
        PreparedStatement ps = con.prepareStatement(selection);
        ps.setString(1,clientLogin);
        ArrayList data = this.listActivitiesSmall(ps);
        return data;
    }


}


