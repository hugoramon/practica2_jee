<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: hugo
  Date: 5/1/20
  Time: 10:41 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sporting Activities Searching Application: Universidad Carlos III de Madrid</title>
</head>
<body>
<c:choose>
    <c:when test="${loginUrl != null}">
        <form action="${loginUrl}" method="post">
    </c:when>
    <c:otherwise>
        <form action="login" method="post">
    </c:otherwise>
</c:choose>

        <div class="container">
            <label for="nick"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="nick" required>
            <br>
            <label for="pass"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="pass" required>
            <br>
            <button type="submit">Login</button>
        </div>
        <div class="container" style="background-color:#f1f1f1">
            <span class="psw"> <a href="register">Registrarse</a></span>
        </div>
        <c:if test="${error != null}">
            <div>
                <p>Usuario o contraseñas incorrectas</p>
            </div>
        </c:if>
    </form>
</body>
</html>
