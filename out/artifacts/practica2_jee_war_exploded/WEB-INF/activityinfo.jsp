<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: hugo
  Date: 5/6/20
  Time: 10:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sporting Activities Searching Application: Universidad Carlos III de Madrid</title>
    <c:if test="${error == null}">
        <p>Name: ${activity.name}</p>
        <p>Description: ${activity.description}</p>
        <p>Start date: ${activity.initial}</p>
        <p>Cost: ${activity.cost}</p>
        <p>Pavilion name: ${activity.pavname}</p>
        <p>Total places: ${activity.total}</p>
        <p>Places already occupied: ${activity.occupied}</p>
    </c:if>

    <c:if test="${error != null}">
        <p>${error}</p>
    </c:if>

    <br>
    <a href="${listUrl}">Go back to the client page</a>
</head>
<body>

</body>
</html>
