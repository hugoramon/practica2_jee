<%--
  Created by IntelliJ IDEA.
  User: hugo
  Date: 4/30/20
  Time: 7:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Sporting Activities Searching Application: Universidad Carlos III de Madrid</title>
    <c:if test="${method == 'get'}">
    <h1> Sporting Activities Searching Application: Universidad Carlos III de Madrid </h1>
    This application allows you visualizing all the sporting activities that are at present in Universidad Carlos III
    de Madrid in its three campus (Getafe, Leganes and Colmenarejo) <p>
    Please select an option <br>
    <div>
        <form action="${urlModAct}" method="post">
            <h2>Subscribe or unsubscribe to an activity</h2>
            <label for="id"><b>ID of the activity</b></label>
            <input type="number" name ="id" placeholder="ID of the activity">
            <div>
                <input type="radio" id="subscribe" name="action" value="subscribe">
                <label for="subscribe">Subscribe</label><br>
                <input type="radio" id="unsubscribe" name="action" value="unsubscribe">
                <label for="unsubscribe">Unsubscribe</label><br>
            </div>
            <button type="submit">Submit</button>
        </form>
    </div>

    <div>

        <form action="${urlAct}" method="get">
            <h2>Get information about an activity</h2>
            <label for="id"><b>ID of the activity</b></label>
            <input type="number" name ="id" placeholder="ID of the activity">
            <button type="submit">Submit</button>
        </form>
    </div>
    <br>

    <h2>List things</h2>
    <form action="${urlList}" method=POST>
        <select name="type">
            <option value="all_activities" SELECTED> List all sporting activities
            <option value="all_pavillions" SELECTED> List all pavillions
            <option value="free_places" SELECTED> List activities for which there are currently free places
            <option value="cost" SELECTED> List activities for which there are free places costing less than a certain amount
            <option value="pavillion" SELECTED> List activities for which there are free places that take place in a certain pavillion
            <option value="sub_actvs" SELECTED>List all the activities where I am subscribed</option>
        </select>
            <br>
            Introduce the cost of the activity or the pavillion name depending on your selection:
            <input type="text" name="text1" size=32><br>
            Order by<select name="order">
            <option value="ID" SELECTED> Activity Id </option>
            <option value="NAME" SELECTED> Activity Name</option>
            <option value="COST" SELECTED> Activity Cost</option>
            <option value="PAVILLION" SELECTED>Pavillion Name</option>
            <option value="LOCATION" SELECTED>Pavillion location</option>
        </select>
            <input type="submit" value="SUBMIT">
            <input type="reset" value="REMOVE">
    </form>
    </c:if>
    <br>
    <br>
    <c:if test="${error != null}">
        <p>An error have occurred</p>
    </c:if>

    <c:if test="${method == 'post'}">
        <c:choose>
            <c:when test="${param.type == 'all_pavillions'}">
                <h1>List of all pavillions</h1>
                <table>
                    <tr><b><td>PAVILLION</td><td>LOCATION</td></b></tr><p>
                    <c:forEach items="${list}" var="record">
                        <tr>
                            <td>${record.name}</td>
                            <td>${record.location }</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <h1>List of activities according to the searching conditions </h1>
                <table>
                    <tr><b><td>ID</td><td>NAME</td><td>DESCRIPTION</td><td>START_DATE</td><td>COST</td><td>PAVILLION_NAME</td><td>TOTAL_PLACES</td><td>OCCUPIED_PLACES</td></b></tr><p>
                    <c:forEach items="${list}" var="record">
                        <tr>
                            <td>${record.id }</td>
                            <td>${record.name }</td>
                            <td>${record.description }</td>
                            <td>${record.initial}</td>
                            <td>${record.cost }</td>
                            <td>${record.pavname }</td>
                            <td>${record.total }</td>
                            <td>${record.occupied}</td>
                        </tr>
                    </c:forEach>
                </table>
                <a href="${urlList}">Click here to go back</a>
            </c:otherwise>
        </c:choose>
    </c:if>
</head>
<body>

</body>
</html>
