<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: hugo
  Date: 4/30/20
  Time: 11:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="true" %>
<html>
<head>
    <title>Sporting Activities Searching Application: Universidad Carlos III de Madrid</title>
</head>
<body>
    <c:choose>
        <c:when test="${validate == null}">
            <form action="register" method="post">
                <div class="container">
                    <label for="nick"><b>Nick</b></label>
                    <input type="text" placeholder="Enter Nick" name="nick" required value="${sessionScope.nick}">
                    <br>
                    <label for="name"><b>Name</b></label>
                    <input type="text" placeholder="Enter Name" name="name" value="${sessionScope.name}" required>
                    <br>
                    <label for="surname"><b>Surname</b></label>
                    <input type="text" placeholder="Enter Surname" name="surname" value="${sessionScope.surname}" required>
                    <br>
                    <label for="psw"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="pass" value="${sessionScope.pass}" required>
                    <br>
                    <label for="address"><b>Address</b></label>
                    <input type="text" placeholder="Enter Address" name="address" value="${sessionScope.address}" required>
                    <br>
                    <label for="phone"><b>Phone</b></label>
                    <input type="text" placeholder="Enter Phone Number" name="phone" value="${sessionScope.phone}" required>
                    <br>
                    <button type="submit" class="registerbtn">Register</button>
                </div>
            </form>
        </c:when>
        <c:otherwise>
            <p> Nick: ${param.nick} </p>
            <p> Name: ${param.name}</p>
            <p> Surname: ${param.surname} </p>
            <p> Password: ${param.pass} </p>
            <p> Address: ${param.address} </p>
            <p> Phone: ${param.phone} </p>
            <form action=${url} method="post">
                <h4>If the parameters are correct click the verify button </h4>
                <input type="hidden" name ="verify" value ="true">
                <button type="submit" class="registerbtn">Register</button>
            </form>
            <form action="${urlBack}" method="get">
                <button type="submit" class="registerbtn">Cancel</button>
            </form>
        </c:otherwise>
    </c:choose>
</body>
</html>
