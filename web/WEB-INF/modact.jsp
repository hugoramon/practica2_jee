<%--
  Created by IntelliJ IDEA.
  User: hugo
  Date: 5/1/20
  Time: 12:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sporting Activities Searching Application: Universidad Carlos III de Madrid</title>

    <c:if test="${message != null}">
        <p>${message}</p>
    </c:if>
    <c:if test="${error != null}">
        <p>${error}</p>
    </c:if>

    <a href="${retunUrl}">
        <p>Return to the client page</p>
    </a>
</head>
<body>

</body>
</html>
